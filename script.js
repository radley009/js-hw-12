const btn = document.querySelectorAll('.btn');

document.addEventListener('keyup', (e) => {
    btn.forEach(elem => {
        elem.style.background = "black";
        if(elem.innerHTML.toLowerCase() === e.key.toLowerCase()) {
            elem.style.background = "blue";
        }
    })
});
